package com.example.firstgame.character;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.example.firstgame.shop.Shop;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDateTime;

import static android.content.Context.MODE_PRIVATE;

public class SavesAndLoads {
    public static void save(Character character, Context context){
        FileOutputStream fos=null;


        String fileName=character.getName()+"_save_";

        try{
            fos=context.openFileOutput(fileName,MODE_PRIVATE);
            fos.write(character.toString().getBytes());


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static Character load(Context context, String name){
        FileInputStream fis=null;
        try{
            fis = context.openFileInput(name+"_save_");
            InputStreamReader isr=new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();


            while (br.readLine()!=null){
                sb.append(br.readLine()).append("\n");
            }


            String[] statsWithName=sb.toString().split(",");
            String[] stats=new String[statsWithName.length];
            for(int i=0;i<statsWithName.length; i++){
                stats[i]=(statsWithName[i].split("="))[1];
            }
            name=stats[0];
            int food=Integer.valueOf(stats[1]);
            int water=Integer.valueOf(stats[2]);
            int energy=Integer.valueOf(stats[3]);
            Boolean disease=Boolean.getBoolean(stats[4]);
            Boolean cold=Boolean.getBoolean(stats[5]);
            Boolean wetting=Boolean.getBoolean(stats[6]);
            Boolean overheat=Boolean.getBoolean(stats[7]);
            Double cashMultiplier=Double.valueOf(stats[8]);
            Double experienceMultiplier=Double.valueOf(stats[9]);
            Boolean healthStatusUpdated=Boolean.getBoolean(stats[10]);
            LocalDateTime lastStatusUpdate=LocalDateTime.parse(stats[11]);
            int cash=Integer.valueOf(stats[12]);
            Boolean winterGear=Boolean.getBoolean(stats[13]);
            Boolean hatAndSunglasses=Boolean.getBoolean(stats[14]);
            Boolean rainJacket=Boolean.getBoolean(stats[15]);
            int amountOfFood=Integer.valueOf(stats[16]);
            int amountOfWater=Integer.valueOf(stats[17]);
            int amountOfCaffee=Integer.valueOf(stats[18].trim());
            int amountOfCure=Integer.valueOf(stats[19].trim());

            Character character=new Character(name,food,water,energy,disease,cold,wetting,overheat,cashMultiplier,experienceMultiplier,healthStatusUpdated,lastStatusUpdate,cash,winterGear,hatAndSunglasses,rainJacket,amountOfFood,amountOfWater,amountOfCaffee,amountOfCure);
            return character;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fis!=null){
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        Character newCharacter=new Character();
        return newCharacter;
    }

    public static void saveShop(String characterName,Shop shop, Context context){
        FileOutputStream fos=null;

        Log.d("zapisany sklep",shop.toString());
        String fileName=characterName+"_save_shop";

        try{
            fos=context.openFileOutput(fileName,MODE_PRIVATE);
            fos.write(shop.toString().getBytes());


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static Shop loadShop(Context context, String name){
        FileInputStream fis=null;
        try{
            fis = context.openFileInput(name+"_save_shop");
            InputStreamReader isr=new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();


            while (br.readLine()!=null){
                sb.append(br.readLine()).append("\n");
            }


            String[] statsWithName=sb.toString().split(",");
            String[] stats=new String[statsWithName.length];
            for(int i=0;i<statsWithName.length; i++){
                stats[i]=(statsWithName[i].split("="))[1];
            }

            int foodPrice=Integer.valueOf(stats[0]);
            int waterPrice=Integer.valueOf(stats[1]);
            int energyPrice=Integer.valueOf(stats[2]);
            int winterGearPrice=Integer.valueOf(stats[3]);
            int hatAndSunglassesPrice=Integer.valueOf(stats[4]);
            int rainJacketPrice=Integer.valueOf(stats[5]);
            int curePrice=Integer.valueOf(stats[6]);
            LocalDateTime lastPriceUpdateDateTime=LocalDateTime.parse(stats[7].trim());


            Shop shop=new Shop(foodPrice,waterPrice,energyPrice,winterGearPrice,hatAndSunglassesPrice,rainJacketPrice,curePrice,lastPriceUpdateDateTime);
            Log.d("wczytany sklep",shop.toString());
            return shop;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fis!=null){
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        Shop newShop=new Shop();
        return newShop;
    }
}
