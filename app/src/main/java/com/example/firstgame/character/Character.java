package com.example.firstgame.character;

import android.content.Context;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDateTime;

import static android.content.Context.MODE_PRIVATE;

public class Character {
    private String name;
    private int food;
    private int water;
    private int energy;
    private boolean disease;
    private boolean cold;
    private boolean wetting;
    private boolean overheat;
    private double cashMultiplier;
    private double experienceMultiplier;
    private boolean healthStatusUpdated;
    private LocalDateTime lastStatusUpdate;
    private int cash;
    private boolean winterGear;
    private boolean hatAndSunglasses;
    private boolean rainJacket;

    //rzeczy ze sklepu
    private int amountOfFood;
    private int amountOfWater;
    private int amountOfCaffee;
    private int amountOfCure;

    public Character() {
        String name="NoName";
        food=50;//
        water=50;//
        energy=100;//
        cash=100;
        disease=false;//
        cold=false;     //dalsza aktualizacja
        overheat=false;     //dalsza aktualizacja
        wetting=false;      //dalsza aktualizacja
        cashMultiplier=1;
        experienceMultiplier=1;
        lastStatusUpdate=LocalDateTime.now();
        winterGear=false;
        hatAndSunglasses=false;
        rainJacket=false;
        amountOfFood=1;
        amountOfWater=1;
        amountOfCaffee=2;
        amountOfCure=0;
    }

    public Character(String name) {
        this.name = name;
        food=50;//
        water=50;//
        energy=100;//
        cash=100;
        disease=false;//
        cold=false;     //dalsza aktualizacja
        overheat=false;     //dalsza aktualizacja
        wetting=false;      //dalsza aktualizacja
        cashMultiplier=1;
        experienceMultiplier=1;
        lastStatusUpdate=LocalDateTime.now();
        winterGear=false;
        hatAndSunglasses=false;
        rainJacket=false;
        amountOfFood=1;
        amountOfWater=1;
        amountOfCaffee=2;
        amountOfCure=0;
    }

    public Character(String name, int food, int water, int energy, boolean disease, boolean cold, boolean wetting, boolean overheat, double cashMultiplier, double experienceMultiplier, boolean healthStatusUpdated, LocalDateTime lastStatusUpdate, int cash, boolean winterGear, boolean hatAndSunglasses, boolean rainJacket, int amountOfFood, int amountOfWater, int amountOfCaffee, int amountOfCure) {
        this.name = name;
        this.food = food;
        this.water = water;
        this.energy = energy;
        this.disease = disease;
        this.cold = cold;
        this.wetting = wetting;
        this.overheat = overheat;
        this.cashMultiplier = cashMultiplier;
        this.experienceMultiplier = experienceMultiplier;
        this.healthStatusUpdated = healthStatusUpdated;
        this.lastStatusUpdate = lastStatusUpdate;
        this.cash = cash;
        this.winterGear = winterGear;
        this.hatAndSunglasses = hatAndSunglasses;
        this.rainJacket = rainJacket;
        this.amountOfFood = amountOfFood;
        this.amountOfWater = amountOfWater;
        this.amountOfCaffee = amountOfCaffee;
        this.amountOfCure=amountOfCure;
    }


    @Override
    public String toString() {
        return
                "\nname=" + name +
                ", food=" + food +
                ", water=" + water +
                ", energy=" + energy +
                ", disease=" + disease +
                ", cold=" + cold +
                ", wetting=" + wetting +
                ", overheat=" + overheat +
                ", cashMultiplier=" + cashMultiplier +
                ", experienceMultiplier=" + experienceMultiplier +
                ", healthStatusUpdated=" + healthStatusUpdated +
                ", lastStatusUpdate=" + lastStatusUpdate +
                ", cash=" + cash +
                ", winterGear=" + winterGear +
                ", hatAndSunglasses=" + hatAndSunglasses +
                ", rainJacket=" + rainJacket +
                ", amountOfFood=" + amountOfFood +
                ", amountOfWater=" + amountOfWater +
                ", amountOfCaffee=" + amountOfCaffee +
                ", amountOfCure=" + amountOfCure;
    }

    public int getAmountOfCure() {
        return amountOfCure;
    }

    public void setAmountOfCure(int amountOfCure) {
        this.amountOfCure = amountOfCure;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAmountOfFood() {
        return amountOfFood;
    }

    public void setAmountOfFood(int amountOfFood) {
        this.amountOfFood = amountOfFood;
    }

    public int getAmountOfWater() {
        return amountOfWater;
    }

    public void setAmountOfWater(int amountOfWater) {
        this.amountOfWater = amountOfWater;
    }

    public int getAmountOfCaffee() {
        return amountOfCaffee;
    }

    public void setAmountOfCaffee(int amountOfCaffee) {
        this.amountOfCaffee = amountOfCaffee;
    }

    public boolean isWetting() {
        return wetting;
    }

    public void setWetting(boolean wetting) {
        this.wetting = wetting;
    }

    public boolean isWinterGear() {
        return winterGear;
    }

    public void setWinterGear(boolean winterGear) {
        this.winterGear = winterGear;
    }

    public boolean isHatAndSunglasses() {
        return hatAndSunglasses;
    }

    public void setHatAndSunglasses(boolean hatAndSunglasses) {
        this.hatAndSunglasses = hatAndSunglasses;
    }

    public boolean isRainJacket() {
        return rainJacket;
    }

    public void setRainJacket(boolean rainJacket) {
        this.rainJacket = rainJacket;
    }

    public int getCash() {
        return cash;
    }

    public void setCash(int cash) {
        this.cash = cash;
    }

    public int getFood() {
        return food;
    }

    public int getEnergy() {
        return energy;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }

    public void setFood(int food) {
        this.food = food;
    }

    public int getWater() {
        return water;
    }

    public void setWater(int water) {
        this.water = water;
    }

    public boolean isDisease() {
        return disease;
    }

    public void setDisease(boolean disease) {
        this.disease = disease;
    }

    public boolean isCold() {
        return cold;
    }

    public void setCold(boolean cold) {
        this.cold = cold;
    }

    public boolean isOverheat() {
        return overheat;
    }

    public void setOverheat(boolean overheat) {
        this.overheat = overheat;
    }

    public double getCashMultiplier() {
        return cashMultiplier;
    }

    public void setCashMultiplier(double cashMultiplier) {
        this.cashMultiplier = cashMultiplier;
    }

    public double getExperienceMultiplier() {
        return experienceMultiplier;
    }

    public void setExperienceMultiplier(double experienceMultiplier) {
        this.experienceMultiplier = experienceMultiplier;
    }

    public boolean isHealthStatusUpdated() {
        return healthStatusUpdated;
    }

    public void setHealthStatusUpdated(boolean healthStatusUpdated) {
        this.healthStatusUpdated = healthStatusUpdated;
    }

    public LocalDateTime getLastStatusUpdate() {
        return lastStatusUpdate;
    }

    public void setLastStatusUpdate(LocalDateTime lastStatusUpdate) {
        this.lastStatusUpdate = lastStatusUpdate;

    }



}









