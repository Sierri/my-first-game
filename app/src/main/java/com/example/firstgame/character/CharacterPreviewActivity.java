package com.example.firstgame.character;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.appcompat.app.AppCompatActivity;

import com.example.firstgame.MainMenu;
import com.example.firstgame.R;
import com.example.firstgame.shop.ShoppingActivity;
import com.google.firebase.auth.FirebaseAuth;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDateTime;

public class CharacterPreviewActivity extends AppCompatActivity {
    private Character character;
    private String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.character_screen_layout);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        name=getIntent().getStringExtra("name");
        character=SavesAndLoads.load(this,name);
        setup();
        Log.d("sprawdzenie",character.toString());



    }

    public void onClickBack(View view) {
        Intent intent =new Intent(this, MainMenu.class);
        SavesAndLoads.save(character,this);
        intent.putExtra("name",character.getName());
        Toast.makeText(this, "Zapisano",
                Toast.LENGTH_SHORT).show();
        this.startActivity(intent);
    }

    public void onClickUseFood(View view) {
        if(character.getAmountOfFood()>0&&character.getFood()<100){
            character.setAmountOfFood(character.getAmountOfFood()-1);
            if(character.getFood()<=80){
            character.setFood(character.getFood()+20);}
            else character.setFood(100);
            SavesAndLoads.save(character,this);
            setup();

        }
        else {
            Toast.makeText(this,"Nie można zjeść racji żywnościowych",Toast.LENGTH_SHORT).show();
        }
        SavesAndLoads.save(character,this);
    }

    public void onClickUseWater(View view) {
        if(character.getAmountOfWater()>0&&character.getWater()<100){
            character.setAmountOfWater(character.getAmountOfWater()-1);
            if(character.getWater()<=80){
                character.setWater(character.getWater()+20);}
            else character.setWater(100);
            SavesAndLoads.save(character,this);
            setup();
        }
        else {
            Toast.makeText(this,"Nie można wypić wody",Toast.LENGTH_SHORT).show();
        }
        SavesAndLoads.save(character,this);
    }

    public void onClickUseCoffee(View view) {
        if(character.getAmountOfCaffee()>0&&character.getEnergy()<100){
            character.setAmountOfCaffee(character.getAmountOfCaffee()-1);
            if(character.getEnergy()<=80){
                character.setEnergy(character.getEnergy()+20);}
            else character.setEnergy(100);
            SavesAndLoads.save(character,this);
            setup();
        }
        else {
            Toast.makeText(this,"Nie można wypić kawy",Toast.LENGTH_SHORT).show();
        }
        SavesAndLoads.save(character,this);
    }

    public void onClickUseCure(View view) {
        if(character.isDisease()&character.getAmountOfCure()>0){
            character.setAmountOfCure(character.getAmountOfCure()-1);
           character.setDisease(false);

            SavesAndLoads.save(character,this);
            setup();
        }
        else {
            Toast.makeText(this,"Nie można użyć leku",Toast.LENGTH_SHORT).show();
        }
        SavesAndLoads.save(character,this);
    }

    public void onClickShopBtn(View view) {
        SavesAndLoads.save(character,this);
        Intent intent=new Intent(this, ShoppingActivity.class);
        intent.putExtra("name",name);
        this.startActivity(intent);
    }


    public void setup(){
        ProgressBar hungryPB=findViewById(R.id.hungryProgressBar);
        ProgressBar waterPB=findViewById(R.id.thirstyProgressBar);
        ProgressBar energyPB=findViewById(R.id.energyProgressBar);

        TextView hungryTV=findViewById(R.id.hungryTextIndicator);
        TextView waterTV=findViewById(R.id.ThirstyTextIndicator);
        TextView energyTV=findViewById(R.id.energyTextIndicator);

        TextView cashTV=findViewById(R.id.cashAmount);

        TextView quantityFoodTV=findViewById(R.id.quantityOfFood);
        TextView quantityWaterTV=findViewById(R.id.quantityOfWater);
        TextView quantityCoffeeTV=findViewById(R.id.quantityOfCoffee);
        TextView quantityCureTV=findViewById(R.id.quantityOfCure);

        ToggleButton hatAndSunglassesBtn=findViewById(R.id.hatAndSunglassesBtn);
        ToggleButton rainJacketBtn=findViewById(R.id.rainJacketBtn);
        ToggleButton winterUniformBtn=findViewById(R.id.winterUniformBtn);
        hatAndSunglassesBtn.setVisibility(View.INVISIBLE);
        rainJacketBtn.setVisibility(View.INVISIBLE);
        winterUniformBtn.setVisibility(View.INVISIBLE);

        hungryPB.setProgress(character.getFood());
        waterPB.setProgress(character.getWater());
        energyPB.setProgress(character.getEnergy());
        String hunger=character.getFood()+"/100";
        String water=character.getWater()+"/100";
        String energy=character.getEnergy()+"/100";
        hungryTV.setText(hunger);
        waterTV.setText(water);
        energyTV.setText(energy);
        cashTV.setText(String.valueOf(character.getCash()));

        String quantityFood="Racje żywnościowe: "+character.getAmountOfFood();
        String quantityWater="Butelki wody: "+character.getAmountOfWater();
        String quantityCaffee="Kawy: "+character.getAmountOfCaffee();
        String quantityCures="Lekarstwa: "+character.getAmountOfCure();
        quantityFoodTV.setText(quantityFood);
        quantityWaterTV.setText(quantityWater);
        quantityCoffeeTV.setText(quantityCaffee);
        quantityCureTV.setText(quantityCures);

        if(character.isHatAndSunglasses()){
            hatAndSunglassesBtn.setVisibility(View.VISIBLE);
        }
        if(character.isRainJacket()){
            rainJacketBtn.setVisibility(View.VISIBLE);
        }
        if(character.isWinterGear()){
            winterUniformBtn.setVisibility(View.VISIBLE);
        }


    }



}
