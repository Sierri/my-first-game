package com.example.firstgame.character;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Random;

public class StatusUpdater {

    public Character characterNewStatus(Character characterBefore){
        LocalDateTime now=LocalDateTime.now();
        LocalDateTime lastUpdate=characterBefore.getLastStatusUpdate();

        Duration duration=Duration.between(now,lastUpdate);
        Long elapsedTime=Math.abs(duration.toMinutes());

        int foodLost;
        int waterLost;
        int energyLost;

        if(characterBefore.isDisease()) {                   //ZMIANY W TRAKCIE CHOROBY
           foodLost = (int) Math.floor(elapsedTime / 6);
           waterLost = (int) Math.floor(elapsedTime / 6);
           energyLost = (int) Math.floor(elapsedTime / 6);


        }else{                                               //GDY ZDROWY
            foodLost = (int) Math.floor(elapsedTime / 12);
            waterLost = (int) Math.floor(elapsedTime / 12);
            energyLost = (int) Math.floor(elapsedTime / 12);
        }

        Character characterAfter=characterBefore;
        characterAfter.setFood(characterBefore.getFood()-foodLost);
        characterAfter.setWater(characterBefore.getWater()-waterLost);
        characterAfter.setEnergy(characterBefore.getEnergy()-energyLost);


        if((!(now.getDayOfMonth()==lastUpdate.getDayOfMonth()))&&!characterBefore.isDisease()){     //WYSTĄPIENIE CHOROBY
            int diseaseChance=5;
            if(characterAfter.getFood()<30)
                diseaseChance+=10;

            if(characterAfter.getWater()<30)
                diseaseChance+=10;

            if (characterAfter.getEnergy()<30)
                diseaseChance+=10;

            if(characterAfter.isCold()||characterAfter.isOverheat()||characterAfter.isWetting())
                diseaseChance+=30;

            Random random=new Random();
            int number=random.nextInt(100);
            if (number<=diseaseChance-1) {
                characterAfter.setDisease(true);
            }

        }

                                //Multiplery
        if(characterAfter.isDisease()){
            characterAfter.setCashMultiplier(0.6);
            characterAfter.setExperienceMultiplier(0.6);
        }else if (characterAfter.isOverheat()||characterAfter.isCold()||characterAfter.isWetting()){
            characterAfter.setCashMultiplier(0.8);
            characterAfter.setExperienceMultiplier(0.8);
        }else{
            characterAfter.setCashMultiplier(1);
            characterAfter.setExperienceMultiplier(1);
        }

        if(characterAfter.getFood()>84&&characterAfter.getEnergy()>84&&characterAfter.getWater()>84){
            characterAfter.setCashMultiplier(1.5);
            characterAfter.setExperienceMultiplier(1.5);
        }




characterAfter.setLastStatusUpdate(LocalDateTime.now());
        return characterAfter;
    }

}
