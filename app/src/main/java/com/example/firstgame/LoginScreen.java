package com.example.firstgame;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.List;


public class LoginScreen extends AppCompatActivity {
    private static final String TAG ="";
    private List<User> userList= new ArrayList<>();

private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);
        mAuth=FirebaseAuth.getInstance();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
    }


    public void onClickLoginBtn(View view) {
        EditText login= findViewById(R.id.loginField);
        EditText password= findViewById(R.id.passwordField);

        String logString=login.getText().toString();
        String passString=password.getText().toString();

        if (logString.isEmpty()||passString.isEmpty()){
            Toast.makeText(LoginScreen.this.getApplicationContext(), "Wypełnij wszystkie pola",
                    Toast.LENGTH_LONG).show();

        }
        else {
            login(logString,passString);
        }
    }

    public void onClickRegisterBtn(View view) {
        EditText login= findViewById(R.id.newLoginField);
        EditText password= findViewById(R.id.newPasswordField);

        String logString=login.getText().toString();
        String passString=password.getText().toString();

        if (logString.isEmpty()||passString.isEmpty()){
            Toast.makeText(LoginScreen.this.getApplicationContext(), "Wypełnij wszystkie pola",
                    Toast.LENGTH_LONG).show();

        }
        else if(passString.length()<6) {
            Toast.makeText(LoginScreen.this.getApplicationContext(), "Hasło musi zawierać przynajmniej 6 znaków",
                    Toast.LENGTH_LONG).show();
        }
        else if(logString.length()<5){
            Toast.makeText(LoginScreen.this.getApplicationContext(), "Login musi zawierać przynajmniej 5 znaków",
                    Toast.LENGTH_LONG).show();
        }
        else {
            register(logString,passString);
        }
    }

    public void login(final String email, String password){
        String name=email.split("@")[0];
     mAuth.signInWithEmailAndPassword(email,password)
             .addOnCompleteListener(this, (task)-> {
            if(task.isSuccessful()){
                Log.d(TAG,"signInWithEmail:success");
                FirebaseUser user = mAuth.getCurrentUser();
                Toast.makeText(getApplicationContext(),"Zalogowano pomyślnie",
                        Toast.LENGTH_LONG).show();
                Intent intent = new Intent(this, MainMenu.class);

                intent.putExtra("name",name);
                this.startActivity(intent);

            }else {
                Log.d(TAG, "signInWithEmail:failure", task.getException());
                Toast.makeText(getApplicationContext(), "Logowanie nie udane. Spróbuj ponownie.",
                        Toast.LENGTH_LONG).show();
                Intent intent=new Intent(this,LoginScreen.class);
                this.startActivity(intent);
            }
        });



    }

    public void register(String email, String password){
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, (task) -> {
                    if (task.isSuccessful()) {
                        Log.d(TAG, "createUserWithEmail:success");
                        Toast.makeText(getApplicationContext(), "Konto zostało zarejestrowane.",
                                Toast.LENGTH_LONG).show();
                        FirebaseUser user = mAuth.getCurrentUser();
                    } else {
                        Log.w(TAG, "CreateUserWithEmail:failure", task.getException());
                        Toast.makeText(getApplicationContext(), "Błąd przy rejestracji konta.",
                                Toast.LENGTH_LONG).show();
                        Intent intent=new Intent(this,LoginScreen.class);
                        this.startActivity(intent);
                    }
                });

        LinearLayout registerLayout=findViewById(R.id.registerLayout);
        Button registerBtn = findViewById(R.id.registerBtn);
        TextView text=findViewById(R.id.textView3);
        registerBtn.setVisibility(View.INVISIBLE);
        registerLayout.setVisibility(View.INVISIBLE);
        text.setVisibility(View.INVISIBLE);
    }
}
