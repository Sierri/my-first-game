package com.example.firstgame.shop;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Shop {
    private int foodPrice;
    private int waterPrice;
    private int caffeePrice;
    private int winterGearPrice;
    private int hatAndSunglassesPrice;
    private int rainJacketPrice;
    private int curePrice;
    private LocalDateTime lastPriceUpdateDateTime;

    public Shop() {
        foodPrice=30;
        waterPrice=30;
        caffeePrice=30;
        winterGearPrice=200;
        hatAndSunglassesPrice=200;
        rainJacketPrice=200;
        curePrice=100;
        lastPriceUpdateDateTime=LocalDateTime.now();
    }

    public Shop(int foodPrice, int waterPrice, int caffeePrice, int winterGearPrice, int hatAndSunglassesPrice, int rainJacketPrice, int curePrice, LocalDateTime lastPriceUpdateDateTime) {
        this.foodPrice = foodPrice;
        this.waterPrice = waterPrice;
        this.caffeePrice = caffeePrice;
        this.winterGearPrice = winterGearPrice;
        this.hatAndSunglassesPrice = hatAndSunglassesPrice;
        this.rainJacketPrice = rainJacketPrice;
        this.curePrice = curePrice;
        this.lastPriceUpdateDateTime = lastPriceUpdateDateTime;
    }

    public LocalDateTime getLastPriceUpdateDateTime() {
        return lastPriceUpdateDateTime;
    }

    public void setLastPriceUpdateDateTime(LocalDateTime lastPriceUpdateDateTime) {
        this.lastPriceUpdateDateTime = lastPriceUpdateDateTime;
    }

    public int getFoodPrice() {
        return foodPrice;
    }

    public void setFoodPrice(int foodPrice) {
        this.foodPrice = foodPrice;
    }

    public int getWaterPrice() {
        return waterPrice;
    }

    public void setWaterPrice(int waterPrice) {
        this.waterPrice = waterPrice;
    }

    public int getCaffeePrice() {
        return caffeePrice;
    }

    public void setCaffeePrice(int caffeePrice) {
        this.caffeePrice = caffeePrice;
    }

    public int getWinterGearPrice() {
        return winterGearPrice;
    }

    public void setWinterGearPrice(int winterGearPrice) {
        this.winterGearPrice = winterGearPrice;
    }

    public int getHatAndSunglassesPrice() {
        return hatAndSunglassesPrice;
    }

    public void setHatAndSunglassesPrice(int hatAndSunglassesPrice) {
        this.hatAndSunglassesPrice = hatAndSunglassesPrice;
    }

    public int getRainJacketPrice() {
        return rainJacketPrice;
    }

    public void setRainJacketPrice(int rainJacketPrice) {
        this.rainJacketPrice = rainJacketPrice;
    }

    public int getCurePrice() {
        return curePrice;
    }

    public void setCurePrice(int curePrice) {
        this.curePrice = curePrice;
    }

    @Override
    public String toString() {
        return
                "\nfoodPrice=" + foodPrice +
                ", waterPrice=" + waterPrice +
                ", caffeePrice=" + caffeePrice +
                ", winterGearPrice=" + winterGearPrice +
                ", hatAndSunglassesPrice=" + hatAndSunglassesPrice +
                ", rainJacketPrice=" + rainJacketPrice +
                ", curePrice=" + curePrice +
                ", lastPriceUpdateDateTime=" + lastPriceUpdateDateTime;
    }
}
