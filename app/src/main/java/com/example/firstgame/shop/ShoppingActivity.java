package com.example.firstgame.shop;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.example.firstgame.MainMenu;
import com.example.firstgame.R;
import com.example.firstgame.character.Character;
import com.example.firstgame.character.CharacterPreviewActivity;
import com.example.firstgame.character.SavesAndLoads;
import com.google.firebase.auth.FirebaseAuth;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Random;

public class ShoppingActivity extends AppCompatActivity {
    private Character character;
    private Shop shop;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.shop_layout);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        String name = getIntent().getStringExtra("name");
        this.character = SavesAndLoads.load(this, name);
        shopSetup();
        setup();

    }

    public void onClickBuyFood(View view) {
        if(character.getCash()>=shop.getFoodPrice()){
character.setCash(character.getCash()-shop.getFoodPrice());
character.setAmountOfFood(character.getAmountOfFood()+1);
SavesAndLoads.save(character,this);
            Toast.makeText(this,"kupiono",Toast.LENGTH_SHORT).show();
setup();
        }
    }

    public void onClickBuyWater(View view) {
        if(character.getCash()>=shop.getWaterPrice()){
            character.setCash(character.getCash()-shop.getWaterPrice());
            character.setAmountOfWater(character.getAmountOfWater()+1);
            SavesAndLoads.save(character,this);
            Toast.makeText(this,"kupiono",Toast.LENGTH_SHORT).show();
            setup();
        }

    }

    public void onClickBuyCoffee(View view) {
        if(character.getCash()>=shop.getCaffeePrice()){
            character.setCash(character.getCash()-shop.getCaffeePrice());
            character.setAmountOfCaffee(character.getAmountOfCaffee()+1);
            SavesAndLoads.save(character,this);
            Toast.makeText(this,"kupiono",Toast.LENGTH_SHORT).show();
            setup();
        }

    }

    public void onClickBuyCure(View view) {
        if(character.getCash()>=shop.getCurePrice()){
            character.setCash(character.getCash()-shop.getCurePrice());
            character.setAmountOfCure(character.getAmountOfCure()+1);
            SavesAndLoads.save(character,this);
            Toast.makeText(this,"kupiono",Toast.LENGTH_SHORT).show();
            setup();
        }
    }

    public void onClickBuyHatAndSunglasses(View view) {
        if(character.getCash()>=shop.getHatAndSunglassesPrice()){
            character.setCash(character.getCash()-shop.getHatAndSunglassesPrice());
            character.setHatAndSunglasses(true);
            SavesAndLoads.save(character,this);
            Toast.makeText(this,"kupiono",Toast.LENGTH_SHORT).show();
            setup();

        }
    }

    public void onClickBuyRainJacket(View view) {
        if(character.getCash()>=shop.getRainJacketPrice()){
            character.setCash(character.getCash()-shop.getRainJacketPrice());
            character.setRainJacket(true);
            SavesAndLoads.save(character,this);
            Toast.makeText(this,"kupiono",Toast.LENGTH_SHORT).show();
            setup();

        }

    }

    public void onClickBuyWinterUniform(View view) {
        if(character.getCash()>=shop.getWinterGearPrice()){
            character.setCash(character.getCash()-shop.getWinterGearPrice());
            character.setWinterGear(true);
            SavesAndLoads.save(character,this);
            Toast.makeText(this,"kupiono",Toast.LENGTH_SHORT).show();
            setup();

        }
    }

    public void onClickCharacterBtn(View view) {
        SavesAndLoads.save(character, this);
        Intent intent = new Intent(this, CharacterPreviewActivity.class);
        intent.putExtra("name", character.getName());
        this.startActivity(intent);
    }

    public void onClickMainMenu(View view) {
        SavesAndLoads.save(character, this);
        Intent intent = new Intent(this, MainMenu.class);
        intent.putExtra("name", character.getName());
        this.startActivity(intent);
    }

    public void shopSetup(){
        Shop oldShop=SavesAndLoads.loadShop(this,character.getName());
        Log.d("shoptime",oldShop.getLastPriceUpdateDateTime().toString());
        Duration duration=Duration.between(LocalDateTime.now(),oldShop.getLastPriceUpdateDateTime());
        Long elapsedTime=Math.abs(duration.toMinutes());
        Log.d("elapsed time",elapsedTime.toString());
        if(elapsedTime<60){
            shop=oldShop;
            SavesAndLoads.saveShop(character.getName(),shop,this);
        }else{
            Random random=new Random();
            int foodPrice=random.nextInt(30)+20;
            int waterPrice=random.nextInt(30)+20;
            int coffeePrice=random.nextInt(30)+20;
            int curePrice=random.nextInt(50)+80;
            int hatPrice=random.nextInt(60)+170;
            int rainJacketPrice=random.nextInt(60)+170;
            int winterGearPrice=random.nextInt(60)+170;
            LocalDateTime updateTime=LocalDateTime.now();
            shop=new Shop(foodPrice,waterPrice,coffeePrice,winterGearPrice,hatPrice,rainJacketPrice,curePrice,updateTime);
            SavesAndLoads.saveShop(character.getName(),shop,this);
        }


    }

    public void setup() {

        //cash
        TextView cash = findViewById(R.id.cashAmount);
        cash.setText("Kasa: " + character.getCash());

        //food
        TextView foodPrice = findViewById(R.id.foodPrice);
        Button buyFoodBtn= findViewById(R.id.buyFoodBtn);
        foodPrice.setText("Cena: " + shop.getFoodPrice());
        if(shop.getFoodPrice()>character.getCash()){
            foodPrice.setTextColor(getResources().getColor(R.color.red));
            buyFoodBtn.setClickable(false);
            buyFoodBtn.setBackground(ContextCompat.getDrawable(this,R.drawable.buy_btn_false));
        }
        TextView foodInEq = findViewById(R.id.foodInEq);
        foodInEq.setText("masz: " + character.getAmountOfFood());

        //water
        TextView waterPrice = findViewById(R.id.waterPrice);
        waterPrice.setText("Cena: " + shop.getWaterPrice());
        Button buyWaterBtn= findViewById(R.id.buyWaterBtn);
        if(shop.getWaterPrice()>character.getCash()){
            waterPrice.setTextColor(getResources().getColor(R.color.red));
            buyWaterBtn.setClickable(false);
            buyWaterBtn.setBackground(ContextCompat.getDrawable(this,R.drawable.buy_btn_false));
        }
        TextView waterInEq = findViewById(R.id.waterInEq);
        waterInEq.setText("masz: " + character.getAmountOfWater());

        //coffee
        TextView coffeePrice = findViewById(R.id.caffeePrice);
        coffeePrice.setText("Cena: " + shop.getCaffeePrice());
        Button buyCoffeeBtn= findViewById(R.id.buyCaffeeBtn);
        if(shop.getCaffeePrice()>character.getCash()){
            coffeePrice.setTextColor(getResources().getColor(R.color.red));
            buyCoffeeBtn.setClickable(false);
            buyCoffeeBtn.setBackground(ContextCompat.getDrawable(this,R.drawable.buy_btn_false));
        }
        TextView coffeeInEq = findViewById(R.id.caffeeInEq);
        coffeeInEq.setText("masz: " + character.getAmountOfCaffee());

        //cure
        TextView curePrice = findViewById(R.id.curePrice);
        curePrice.setText("Cena: " + shop.getCurePrice());
        Button buyCureBtn= findViewById(R.id.buyCureBtn);
        if(shop.getCurePrice()>character.getCash()){
            curePrice.setTextColor(getResources().getColor(R.color.red));
            buyCureBtn.setClickable(false);
            buyCureBtn.setBackground(ContextCompat.getDrawable(this,R.drawable.buy_btn_false));
        }
        TextView cureInEq = findViewById(R.id.cureInEq);
        cureInEq.setText("masz: " + character.getAmountOfCure());

        //hatAndSunglasses
        TextView hatAndSunglassesPrice = findViewById(R.id.hatAndSunglassesPrice);
        hatAndSunglassesPrice.setText("Cena: " + shop.getHatAndSunglassesPrice());
        TextView buyHatBtn = findViewById(R.id.buyHatAndSunglasses);

        if(shop.getHatAndSunglassesPrice()>character.getCash()){
            hatAndSunglassesPrice.setTextColor(getResources().getColor(R.color.red));
            buyHatBtn.setClickable(false);
            buyHatBtn.setBackground(ContextCompat.getDrawable(this,R.drawable.buy_btn_false));
        }
        TextView hatAndSunglassesInEq = findViewById(R.id.hatAndSunglassesInEq);
        if (character.isHatAndSunglasses()) {
            hatAndSunglassesInEq.setVisibility(View.VISIBLE);
            buyHatBtn.setClickable(false);
            buyHatBtn.setBackground(ContextCompat.getDrawable(this,R.drawable.buy_btn_false));
        } else {
            hatAndSunglassesInEq.setVisibility(View.INVISIBLE);
        }


        //rainJacket
        TextView rainJacketPrice = findViewById(R.id.rainJacketPrice);
        rainJacketPrice.setText("Cena: " + shop.getRainJacketPrice());
        TextView buyRainJacketBtn = findViewById(R.id.buyRainJacket);

        if(shop.getRainJacketPrice()>character.getCash()){
            rainJacketPrice.setTextColor(getResources().getColor(R.color.red));
            buyRainJacketBtn.setClickable(false);
            buyRainJacketBtn.setBackground(getDrawable(R.drawable.buy_btn_false));
        }
        TextView rainJacketInEq = findViewById(R.id.rainJacketInEq);
        if (character.isRainJacket()) {
            rainJacketInEq.setVisibility(View.VISIBLE);
            buyRainJacketBtn.setClickable(false);
            buyRainJacketBtn.setBackground(getDrawable(R.drawable.buy_btn_false));
        } else {
            rainJacketInEq.setVisibility(View.INVISIBLE);
        }

        //winterUniform
        TextView winterUniformPrice = findViewById(R.id.winterUniformPrice);
        winterUniformPrice.setText("Cena: " + shop.getWinterGearPrice());
        TextView buyWinterUniformBtn = findViewById(R.id.buyWinterUniform);

        if(shop.getWinterGearPrice()>character.getCash()){
            winterUniformPrice.setTextColor(getResources().getColor(R.color.red));
            buyWinterUniformBtn.setClickable(false);
            buyWinterUniformBtn.setBackground(getDrawable(R.drawable.buy_btn_false));
        }
        TextView winterUniformInEq = findViewById(R.id.winterUniformInEq);
        if (character.isWinterGear()) {
            winterUniformInEq.setVisibility(View.VISIBLE);
            buyWinterUniformBtn.setClickable(false);
            buyWinterUniformBtn.setBackground(getDrawable(R.drawable.buy_btn_false));
        } else {
            winterUniformInEq.setVisibility(View.INVISIBLE);
        }

    }
}

