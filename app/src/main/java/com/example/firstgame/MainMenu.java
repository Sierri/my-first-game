package com.example.firstgame;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.firstgame.character.Character;
import com.example.firstgame.character.CharacterPreviewActivity;
import com.example.firstgame.character.SavesAndLoads;
import com.example.firstgame.games.MathGameCategory;
import com.example.firstgame.shop.ShoppingActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Random;

public class MainMenu extends AppCompatActivity {
    private FirebaseFirestore nFirestore=FirebaseFirestore.getInstance();
    private FirebaseAuth afFirestore = FirebaseAuth.getInstance();
    private Character character;
    private String name;
    private String userID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_menu);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        userID= Objects.requireNonNull(afFirestore.getCurrentUser().getUid());

        name = getIntent().getStringExtra("name");
        character=load();
        character.setName(name);
        setup();
        //character=load();



    }

    public void onClickCharacterBtn(View view) {
        Intent intent=new Intent(this, CharacterPreviewActivity.class);
        intent.putExtra("name",name);
        save();
        this.startActivity(intent);
    }

    public void onClickGame1(View view) {   //matematyka
        Intent intent=new Intent(this, MathGameCategory.class);
        intent.putExtra("name",name);
        save();
        this.startActivity(intent);

    }

    public void onClickGame2(View view) {   //logiczne
        save();
    }

    public void onClickGame3(View view) {   //pamięć
        save();
    }

    public void onClickGame4(View view) {   //wiedza
        save();
    }

    public void setup(){

        TextView welcomeText=findViewById(R.id.welcomeAfterLogin);
        String s1="Cześć "+name;
        String s2="Co u Ciebie słychać "+name+ "?";
        String s3="W co dzisiaj zagramy "+name+"?";
        String[] welcomeTexts={s1,s2,s3};

        Random random=new Random();
        int choosenWelcomeTextInt=random.nextInt(welcomeTexts.length);
        welcomeText.setText(welcomeTexts[choosenWelcomeTextInt]);

        ProgressBar hungryPb=findViewById(R.id.hungryProgressBar);
        ProgressBar waterPb=findViewById(R.id.waterProgressBar);
        ProgressBar energyPb=findViewById(R.id.energyProgressBar);
        hungryPb.setProgress(character.getFood());
        energyPb.setProgress(character.getEnergy());
        waterPb.setProgress(character.getWater());
        TextView cashText = findViewById(R.id.cash);
        cashText.setText("Kasa: "+character.getCash());

        String effects;
        if(character.isDisease()){
           effects="choroba";
        }else if (character.isWetting()){
            effects="przemoczenie";
        }else if (character.isOverheat()){
            effects="przegrzanie";
        }else if (character.isCold()){
            effects="przeziębienie";
        }else{
            effects="brak efektów";
        }
        TextView statusText=findViewById(R.id.statusText);
        statusText.setText(effects);

        TextView multiplierText=findViewById(R.id.multipliersText);
        String text="mnożnik doświadczenia: "+character.getExperienceMultiplier()+
                "\nmnożnik pieniędzy: "+character.getCashMultiplier();
        multiplierText.setText(text);


    }

    public void onClickShopBtn(View view) {
        SavesAndLoads.save(character,this);
        Intent intent=new Intent(this, ShoppingActivity.class);
        intent.putExtra("name",name);
        this.startActivity(intent);
    }

    public void onClickLogOut(View view) {
        Intent intent = new Intent(this,LoginScreen.class);
        save();
        this.startActivity(intent);
    }

    public void save(){
        FileOutputStream fos=null;

        String fileName=name+"_save_";

        try{
            fos=openFileOutput(fileName,MODE_PRIVATE);
            fos.write(character.toString().getBytes());
            Toast.makeText(this, "Zapisano",
                    Toast.LENGTH_LONG).show();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public Character load(){
FileInputStream fis=null;
try{
    fis = openFileInput(name+"_save_");
    InputStreamReader isr=new InputStreamReader(fis);
    BufferedReader br = new BufferedReader(isr);
    StringBuilder sb = new StringBuilder();


    while (br.readLine()!=null){
        sb.append(br.readLine()).append("\n");
    }


    String[] statsWithName=sb.toString().split(",");
    String[] stats=new String[statsWithName.length];
    for(int i=0;i<statsWithName.length; i++){
        stats[i]=(statsWithName[i].split("="))[1];
    }
    String name=stats[0];
    int food=Integer.valueOf(stats[1]);
    int water=Integer.valueOf(stats[2]);
    int energy=Integer.valueOf(stats[3]);
    Boolean disease=Boolean.getBoolean(stats[4]);
    Boolean cold=Boolean.getBoolean(stats[5]);
    Boolean wetting=Boolean.getBoolean(stats[6]);
    Boolean overheat=Boolean.getBoolean(stats[7]);
    Double cashMultiplier=Double.valueOf(stats[8]);
    Double experienceMultiplier=Double.valueOf(stats[9]);
    Boolean healthStatusUpdated=Boolean.getBoolean(stats[10]);
    LocalDateTime lastStatusUpdate=LocalDateTime.parse(stats[11]);
    int cash=Integer.valueOf(stats[12]);
    Boolean winterGear=Boolean.getBoolean(stats[13]);
    Boolean hatAndSunglasses=Boolean.getBoolean(stats[14]);
    Boolean rainJacket=Boolean.getBoolean(stats[15]);
    int amountOfFood=Integer.valueOf(stats[16]);
    int amountOfWater=Integer.valueOf(stats[17]);
    int amountOfCaffee=Integer.valueOf(stats[18].trim());
    int amountOfCure=Integer.valueOf(stats[19].trim());


    Character oldCharacter=new Character(name,food,water,energy,disease,cold,wetting,overheat,cashMultiplier,experienceMultiplier,healthStatusUpdated,lastStatusUpdate,cash,winterGear,hatAndSunglasses,rainJacket,amountOfFood,amountOfWater,amountOfCaffee,amountOfCure);
return oldCharacter;
} catch (FileNotFoundException e) {
    e.printStackTrace();
} catch (IOException e) {
    e.printStackTrace();
} finally {
    if (fis!=null){
        try {
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
Character newCharacter=new Character(getIntent().getStringExtra("name"));
return newCharacter;
    }







    public void downloadCharacter(){
        nFirestore.collection("USERS").document(userID).collection("CHARACTER")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if(task.isSuccessful()){
for (QueryDocumentSnapshot newChar : task.getResult()){
    String name=newChar.get("name").toString();

          }
                        }
                    }
                });
    }

    public void uploadCharacter(){

        Map<String,String> stringMap=new HashMap<>();

        stringMap.put("name",name);
        stringMap.put("hunger",Integer.toString(character.getFood()));
        stringMap.put("thirst",Integer.toString(character.getWater()));
        stringMap.put("energy",Integer.toString(character.getEnergy()));
        stringMap.put("disease",Boolean.toString(character.isDisease()));
        stringMap.put("cold",Boolean.toString(character.isCold()));
        stringMap.put("wetting",Boolean.toString(character.isWetting()));
        stringMap.put("overheat",Boolean.toString(character.isOverheat()));
        stringMap.put("healthStatusUpdated",Boolean.toString(character.isHealthStatusUpdated()));
        stringMap.put("cashMultiplier",Double.toString(character.getCashMultiplier()));
        stringMap.put("experienceMultiplier",Double.toString(character.getExperienceMultiplier()));
        stringMap.put("lastStatusUpdate",character.getLastStatusUpdate().toString());
        stringMap.put("cash",Integer.toString(character.getCash()));
        stringMap.put("winterGear",Boolean.toString(character.isWinterGear()));
        stringMap.put("hatAndSunglasses",Boolean.toString(character.isHatAndSunglasses()));
        stringMap.put("rainJacket",Boolean.toString(character.isRainJacket()));
        stringMap.put("amountOfFood",Integer.toString(character.getAmountOfFood()));
        stringMap.put("amountOfWater",Integer.toString(character.getAmountOfWater()));
        stringMap.put("amountOfCaffee",Integer.toString(character.getAmountOfCaffee()));


        DocumentReference documentReference = nFirestore.collection("USERS").document(userID);
        documentReference.collection("CHARACTER").add(stringMap)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Toast.makeText(getApplicationContext(), "Add data to database:success",
                                Toast.LENGTH_LONG).show();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                String error = e.getMessage();
                Toast.makeText(getApplicationContext(), "Error" + error,
                        Toast.LENGTH_LONG).show();
            }
        });

    }
}
